package com.qingzhu.component.lock.common.exception;

/**
 * 配置异常
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/19 14:26
 */
public class LockConfigException extends IllegalStateException {

    public LockConfigException(String s) {
        super(s);
    }
}
