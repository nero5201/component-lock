package com.qingzhu.component.lock.common.constant.capacity;

/**
 * 为了保证锁对象容量不扩容的算法策略
 * @author xiangjz
 * @version 1.0
 * @date 2021/2/22 9:51
 */
public class StrategyForLoclCapacityConstant {

    public static final String STRATEGY_LFU = "lfu";
    public static final String STRATEGY_LRU = "lru";
}
