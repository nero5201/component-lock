package com.qingzhu.component.lock.common.config.enums.spi;

import com.qingzhu.component.lock.common.constant.cache.strategy.LockCacheOutStrategyTypeConstant;
import com.qingzhu.component.lock.common.constant.locktype.LockTypeConstant;

/**
 * springboot的spi扩展枚举配置类
 * @author xiangjz
 * @version 1.0
 * @date 2021/2/8 14:01
 */
public class SpringbootSpiConfigEnums {

    /**
     * lock.type 的枚举配置类
     */
    public enum LockTypeEnum {
        JUC(LockTypeConstant.LOCK_TYPE_JUC),
        REDIS(LockTypeConstant.LOCK_TYPE_REDIS),
        ZOOKEEPER(LockTypeConstant.LOCK_TYPE_ZOOKEEPER);

        private String type;

        LockTypeEnum(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    /**
     * lock.redis 的redis server 模式举配置类
     */
    public enum LockRedisServerModeEnum {
        SINGLE("single"),
        SENTINEL("sentinel"),
        CLUSTER("cluster");

        private String mode;

        LockRedisServerModeEnum(String mode) {
            this.mode = mode;
        }

        public String getMode() {
            return mode;
        }
    }

    /**
     * lock.strategyForCapacity 保持锁capacity的策略，默认是lfu
     */
    public enum LockStrategyForCapacityEnum {
        LFU(LockCacheOutStrategyTypeConstant.LOCK_CACHE_OUT_STRATEGY_LFU),
        LRU(LockCacheOutStrategyTypeConstant.LOCK_CACHE_OUT_STRATEGY_LRU);

        private String strategy;

        LockStrategyForCapacityEnum(String strategy) {
            this.strategy = strategy;
        }

        public String getStrategy() {
            return strategy;
        }
    }
}
