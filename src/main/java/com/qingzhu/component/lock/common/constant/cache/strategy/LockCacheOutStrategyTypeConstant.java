package com.qingzhu.component.lock.common.constant.cache.strategy;

/**
 * 锁对象淘汰策略类型常量
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/22 10:13
 */
public class LockCacheOutStrategyTypeConstant {

    /**
     * lfu
     */
    public final static String LOCK_CACHE_OUT_STRATEGY_LFU = "lfu";

    /**
     * lru
     */
    public final static String LOCK_CACHE_OUT_STRATEGY_LRU = "lru";

}
