package com.qingzhu.component.lock.common.cache;

import com.qingzhu.component.lock.common.entity.LockEntity;

/**
 * 业务锁的缓存抽象
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/14 17:16
 */
public interface LockCache<K> {

    /**
     * 缓存业务与锁的映射关系
     * @param key
     * @param lock
     */
    LockEntity cache(K key, LockEntity lock);

    /**
     * 缓存业务与锁的映射关系
     * 如果不存在才新增，返回锁对象
     * @param key
     * @param lock
     */
    LockEntity cacheIfAbsent(K key, LockEntity lock);

    /**
     * 移除缓存
     * @param key
     */
    void removeCache(K key);

    /**
     * 获取缓存
     * @param key
     */
    LockEntity get(K key);

    /**
     * 获取缓存的最大容量
     * @return
     */
    default int getCacheCapacity() {
        return 0;
    }

    /**
     * 获取缓存的锁对象数量
     * @return
     */
    default int getCacheSize() {
        return 0;
    }
}
