package com.qingzhu.component.lock.common.util;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Map;

/**
 * 主要工具
 *
 * @author xiangjz
 */
public class BaseUtil {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @SuppressWarnings("rawtypes")
	public static boolean isEmpty(Object obj) {
    	if (obj instanceof Collection) {
            Collection list = (Collection) obj;
    		return list == null || list.isEmpty();
    	} else if(obj instanceof Map) {
    	    Map map = (Map) obj;
            return map == null || map.isEmpty();
        } else if(obj instanceof String) {
    	    String str = (String) obj;
    	    return str == null || str.isEmpty();
        } else {
    		return obj == null;
    	}
    }
    
    public static String removeLastChar(String str){
    	return isEmpty(str)?"":str.substring(0, str.length()-1);
    }
    /**
     * Object转为String
     * @param obj
     * @return
     */
    public static String retStr(Object obj) {
        return isEmpty(obj) ? "" : obj.toString();
    }

    public static String retStrIfNull(Object obj, Object defaultValue) {
        return isEmpty(obj) ? retStr(defaultValue) : retStr(obj);
    }

    /**
     * Object转为Integer
     * @param obj
     * @return
     */
    public static Integer retInt(Object obj){
        if(obj instanceof Boolean)
            return ((boolean) obj) ? 1 : 0;

    	return isEmpty(obj) ? 0 : Integer.parseInt(obj.toString());
    }
    
    /**
     * 将字符串末尾指定的串截取
     * @param str
     * @param suffix
     * @return
     */
    public static String cutStrEndWithChar(String str,String suffix){
    	suffix = isEmpty(suffix)?",":suffix;
    	if(isEmpty(str)){
    		return "";
    	}else{
    		return str.endsWith(suffix)?str.substring(0,str.length()-1):str;
    	}
    }

    /**
     * 下划线格式字符串转换为驼峰格式字符串
     * 
     * @param param
     * @return
     */
    public static String underlineToCamel(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (c == '_') {
                if (++i < len) {
                    sb.append(Character.toUpperCase(param.charAt(i)));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String lowerFirst(String oldStr){
        char[]chars = oldStr.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }
}