package com.qingzhu.component.lock.common.config;

import com.qingzhu.component.lock.common.constant.AutoConfig;
import com.qingzhu.component.lock.common.constant.cache.strategy.LockCacheOutStrategyTypeConstant;
import com.qingzhu.component.lock.common.constant.locktype.LockTypeConstant;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * 自定义配置的实体属性
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/15 9:23
 */
@ConfigurationProperties(prefix = AutoConfig.CONFIG_PREFIX)
@Data
public class LockProperties {

    /**
     * 当前工程所使用的锁类型
     */
    private String type = LockTypeConstant.LOCK_TYPE_JUC; // 默认为非分布式 juc

    /**
     * 锁对象淘汰策略
     */
    private String strategyForCapacity = LockCacheOutStrategyTypeConstant.LOCK_CACHE_OUT_STRATEGY_LFU; // 默认为lfu算法

    /**
     * 这是用来控制主线程fork出子线程总体数量的参数
     * 因为每调用业务接口之前，在lock之后，主线程会fork出一个子线程去执行业务逻辑，而主线程处于阻塞状态
     * 这样做的目的是便于控制处于锁状态下业务逻辑的执行时长等
     * 所以需要控制进程中总体的线程数
     * 一般来说，每一个锁对应一个key，需要参考应用中key的总数，然后结合当前服务器和应用资源合理设置
     */
    private int forkTaskQueueSize = 128; // 默认fork总的线程数为128

    /**
     * juc的相关参数配置
     */
    private Map<String, Object> juc;

    /**
     * redis的相关参数配置
     */
    private Map<String, Object> redis;

    /**
     * zookeeper的相关参数配置
     */
    private Map<String, Object> zookeeper;
}
