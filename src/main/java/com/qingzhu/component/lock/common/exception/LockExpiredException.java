package com.qingzhu.component.lock.common.exception;

/**
 * 锁过期异常，用于通过注解指定加锁最长时间，到期发现业务没有完成的时候抛出的异常
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/19 14:26
 */
public class LockExpiredException extends IllegalStateException {

    public LockExpiredException(String s) {
        super(s);
    }
}
