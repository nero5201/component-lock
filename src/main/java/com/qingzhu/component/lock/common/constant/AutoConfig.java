package com.qingzhu.component.lock.common.constant;

/**
 * 自动装配常量类
 * @author xiangjz
 * @version 1.0
 * @date 2020/8/27 13:49
 */
public class AutoConfig {

    /**
     * lock.key的前缀
     */
    public static final String COMPONENT_LOCK_KEY_PREFIX = "lock-component-1.0.0-";
    /**
     * lock.key的自定义参数分隔符
     */
    public static final String COMPONENT_LOCK_KEY_SPLIT_CHARACTER = "#";

    public static final String COMPONENT_LOCK_LOG_PREFIX = "[lock component]";

    public static final String COMPONENT_PACKAGE_PATH = "com.qingzhu.component.lock.*";
    public static final String COMPONENT_ANNOTATION_PATH = "com.qingzhu.component.lock.annotation.LockCommonBeanAnnotation";

    public static final String CONFIG_PREFIX = "lock";
}
