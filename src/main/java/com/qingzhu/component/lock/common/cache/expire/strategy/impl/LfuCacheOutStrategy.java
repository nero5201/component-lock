package com.qingzhu.component.lock.common.cache.expire.strategy.impl;

import com.qingzhu.component.lock.annotation.strategy.LockCacheOutStrategyBean;
import com.qingzhu.component.lock.common.cache.LockCache;
import com.qingzhu.component.lock.common.cache.expire.strategy.CacheOutStrategy;
import com.qingzhu.component.lock.common.constant.cache.strategy.LockCacheOutStrategyTypeConstant;
import com.qingzhu.component.lock.common.util.BaseUtil;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 缓存淘汰策略：LFU算法
 * @author xiangjz
 * @version 1.0
 * @date 2021/3/12 13:56
 */
@LockCacheOutStrategyBean(value = "lfuCacheOutStrategy", type = LockCacheOutStrategyTypeConstant.LOCK_CACHE_OUT_STRATEGY_LFU)
public class LfuCacheOutStrategy extends CacheOutStrategy {

    // cacheKey <-> 锁使用情况  用于lfu算法
    private Map<Object, LfuCacheOutStrategy.LockUse> lockUseCounter;

    public LfuCacheOutStrategy(LockCache lockCache) {
        super(lockCache);
        this.lockUseCounter = new ConcurrentHashMap<>(super.cacheCapacity);
    }

    @Override
    public void check(Object key) {
        /*
         若lockUseCounter容器超过了（一般不会超过，因为有semaphore限流）或者等于了capacity，需要执行lfu算法来缩容
         移除掉最近最少使用频次的锁对象，容器中只保留使用频次最高的锁对象
         */
        if(!lockUseCounter.containsKey(key) && this.lockUseCounter.size() >= super.cacheCapacity - (super.cacheCapacity >>> 2) - 1) {
            synchronized (this.getClass()) {
                if(this.lockUseCounter.size() >= super.cacheCapacity - (super.cacheCapacity >>> 2) - 1)
                    this.lfu();
            }
        }
        this.lockUseCounter.put(key, this.lockUseAdd(key, this.lockUseCounter.get(key)));
    }

    private LfuCacheOutStrategy.LockUse lockUseAdd(Object key, LfuCacheOutStrategy.LockUse lockUse) {
        if(BaseUtil.isEmpty(lockUse)) {
            lockUse = new LfuCacheOutStrategy.LockUse(key, new AtomicInteger(0), System.currentTimeMillis());
        }
        lockUse.addUseCount();
        return lockUse;
    }

    /**
     * 锁使用频次类，用于lfu算法
     */
    private class LockUse implements Comparable<LfuCacheOutStrategy.LockUse> {
        private Object key; // 锁key
        private AtomicInteger useCount; // 锁对象的访问次数，用于lfu算法依据
        private long time;

        public LockUse(Object key, AtomicInteger useCount, long time) {
            this.key = key;
            this.useCount = useCount;
            this.time = time;
        }

        public AtomicInteger addUseCount() {
            if(BaseUtil.isEmpty(this.useCount)) {
                this.useCount = new AtomicInteger(0);
            }
            // 增加访问频次
            this.useCount.incrementAndGet();
            return this.useCount;
        }

        @Override
        public int compareTo(LfuCacheOutStrategy.LockUse o) {
            int compare = Integer.compare(this.useCount.get(), o.useCount.get());
            return compare == 0 ? Long.compare(this.time, o.time) : compare;
        }
    }

    /**
     * 最近最少频次使用的算法，用于保证lockCache总量不变(防止扩容造成性能降低)的情况下，清除一些缓存
     */
    private void lfu() {
        // 对lockUseCounter进行排序，直接清除掉使用频次相对少的锁
        int lockCacheSize = this.lockUseCounter.size();
        if(lockCacheSize >= super.cacheCapacity - (super.cacheCapacity >>> 2) - 1) {
            // 当前容量超过了负载因子所标记容量，则清理当前容量一半的数据
            LfuCacheOutStrategy.LockUse[] lockUseTemplate = new LfuCacheOutStrategy.LockUse[lockCacheSize];
            List<LockUse> lockUserList = Arrays.asList(this.lockUseCounter.values().toArray(lockUseTemplate));
            Collections.sort(lockUserList);
            for (LfuCacheOutStrategy.LockUse lockUse : new ArrayList<>(lockUserList.subList(0, lockCacheSize >>> 1))) {
                this.lockUseCounter.remove(lockUse.key);
                this.lockCache.removeCache(lockUse.key);
            }
        }
    }

}
