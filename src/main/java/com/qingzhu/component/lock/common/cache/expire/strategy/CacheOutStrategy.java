package com.qingzhu.component.lock.common.cache.expire.strategy;

import com.qingzhu.component.lock.common.cache.LockCache;
import lombok.Data;

/**
 * 缓存淘汰策略
 * @author xiangjz
 * @version 1.0
 * @date 2021/3/12 13:55
 */
@Data
public abstract class CacheOutStrategy {

    protected final int cacheCapacity;
    protected final LockCache lockCache;

    public CacheOutStrategy(LockCache lockCache) {
        this.lockCache = lockCache;
        this.cacheCapacity = lockCache.getCacheCapacity();
    }

    /**
     * 检查是否需要执行淘汰逻辑
     * @param key
     */
    public abstract void check(Object key);
}
