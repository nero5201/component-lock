package com.qingzhu.component.lock.common.constant.locktype;

/**
 * 锁的种类常量
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/22 9:51
 */
public class LockTypeConstant {

    public static final String LOCK_TYPE_JUC = "juc";
    public static final String LOCK_TYPE_REDIS = "redis";
    public static final String LOCK_TYPE_ZOOKEEPER = "zookeeper";
}
