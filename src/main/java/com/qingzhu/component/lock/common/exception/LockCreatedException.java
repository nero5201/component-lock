package com.qingzhu.component.lock.common.exception;

/**
 * 锁创建异常，用于创建锁(比如zk创建节点)时候抛出标记的异常
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/19 14:26
 */
public class LockCreatedException extends IllegalStateException {

    public LockCreatedException(String s) {
        super(s);
    }
}
