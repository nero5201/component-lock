package com.qingzhu.component.lock.common.cache.expire.strategy.impl;

import com.qingzhu.component.lock.annotation.strategy.LockCacheOutStrategyBean;
import com.qingzhu.component.lock.common.cache.LockCache;
import com.qingzhu.component.lock.common.cache.expire.strategy.CacheOutStrategy;
import com.qingzhu.component.lock.common.constant.cache.strategy.LockCacheOutStrategyTypeConstant;
import com.qingzhu.component.lock.common.util.BaseUtil;

import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存淘汰策略：LRU算法
 * @author xiangjz
 * @version 1.0
 * @date 2021/3/12 13:56
 */
@LockCacheOutStrategyBean(value = "lruCacheOutStrategy", type = LockCacheOutStrategyTypeConstant.LOCK_CACHE_OUT_STRATEGY_LRU)
public class LruCacheOutStrategy extends CacheOutStrategy {

    // 链表节点移动时的锁，由于本类是单例，所以就这样定义了
    private final Object linkQueueLock;
    // cacheKey <-> 锁使用情况  用于lfu算法
    private Map<Object, LockRecent> LockRecentCounter;
    // 头指针
    private LockRecent head;
    // 尾指针
    private LockRecent tail;

    public LruCacheOutStrategy(LockCache lockCache) {
        super(lockCache);
        linkQueueLock = new Object();
        this.LockRecentCounter = new ConcurrentHashMap<>(super.cacheCapacity);
    }

    @Override
    public void check(Object key) {
        /*
         若LockRecentCounter容器超过了或者等于了capacity，需要执行lru算法来缩容
         移除掉最远时间使用的锁对象，容器中只保留最近使用过的锁对象
         */
        // 如果到达了扩容的临界点，则跑一次lru算法
        if(!LockRecentCounter.containsKey(key) && this.LockRecentCounter.size() >= super.cacheCapacity - (super.cacheCapacity >>> 2) - 1) {
            synchronized (this.getClass()) {
                if(this.LockRecentCounter.size() >= super.cacheCapacity - (super.cacheCapacity >>> 2) - 1)
                    this.lru();
            }
        }

        // 这里移动节点到链表的头节点，并放入缓存
        this.LockRecentCounter.put(key, this.LockRecentAdd(key, this.LockRecentCounter.get(key)));
    }

    private LockRecent LockRecentAdd(Object key, LockRecent lockRecent) {
        if(BaseUtil.isEmpty(lockRecent)) {
            lockRecent = new LockRecent(key, System.currentTimeMillis());
        }
        synchronized (linkQueueLock) {
            // 这里需要移动节点到链表的头节点，需要加锁
            if(head == null) {
                head = tail = lockRecent;
            } else if(lockRecent == head) {
                // do nothing
            } else if(lockRecent.prev == null && lockRecent.next == null) {
                // 如果对象是新产生的，则直接添加到head即可
                LockRecent temp = head;
                head = lockRecent;
                head.next = temp;
                temp.prev = head;
            } else {
                // 如果是一个已存在的节点，需要填补空缺位置
                lockRecent.prev.next = lockRecent.next;
                if(lockRecent != tail) {
                    lockRecent.next.prev = lockRecent.prev;
                } else {
                    tail = lockRecent.prev;
                }
                // 然后需要将其移动到head
                LockRecent temp = head;
                head = lockRecent;
                head.next = temp;
                temp.prev = head;
                head.prev = null;
            }
        }
        return lockRecent;
    }

    /**
     * 锁使用类，用于lru算法
     */
    private class LockRecent extends SoftReference {
        private Object key; // 锁key
        private LockRecent prev;
        private LockRecent next;
        private long time;

        public LockRecent(Object key, long time) {
            super(key);
            this.key = key;
            this.time = time;
        }
    }

    /**
     * 最近使用的算法，用于保证lockCache总量不变(防止扩容造成性能降低)的情况下，清除一些缓存
     */
    private void lru() {
        // 直接从tail开始，清除一般的数据
        int lockCacheSize = this.LockRecentCounter.size();
        if(lockCacheSize >= super.cacheCapacity - (super.cacheCapacity >>> 2) - 1) {
            // 当前容量超过了负载因子所标记容量，则清理当前容量一半的数据
            int clearSize = lockCacheSize >>> 1;
            LockRecent brokenOne = tail;
            while(clearSize > 0) {
                LockRecentCounter.remove(brokenOne.key);
                brokenOne = brokenOne.prev;
                clearSize --;
            }
            // 直接从brokenOne处断开连接即可，下次gc会清除掉
            brokenOne.next.prev = null;
            brokenOne.next = null;
            tail = brokenOne;
        }
    }

}
