package com.qingzhu.component.lock.common.config.enums;

import com.qingzhu.component.lock.common.constant.capacity.StrategyForLoclCapacityConstant;

/**
 * 为了保证锁对象容量不扩容的算法策略配置枚举类
 * @author xiangjz
 * @version 1.0
 * @date 2021/2/22 10:10
 */
public enum StrategyForCapacityEnumConfig {

    /**
     * lfu算法
     */
    LFU (StrategyForLoclCapacityConstant.STRATEGY_LFU),

    /**
     * lru算法
     */
    LRU (StrategyForLoclCapacityConstant.STRATEGY_LRU)
    ;


    private String strategy;

    StrategyForCapacityEnumConfig(String strategy) {
        this.strategy = strategy;
    }

    public String getStrategy() {
        return strategy;
    }
}
