package com.qingzhu.component.lock.common.config.enums;

import com.qingzhu.component.lock.common.exception.LockConfigException;
import com.qingzhu.component.lock.common.util.BaseUtil;

import java.util.Map;

/**
 * redis的默认配置枚举类
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/20 10:10
 */
public enum RedisEnumConfig {

    /**
     * redis的密码
     */
    CONNECTION_PASSWORD ("password", "", true);

    private String key; // 配置的key
    private Object value; // 配置的value
    private boolean nullable; // 是否可以为空

    RedisEnumConfig(String key, Object value, boolean nullable) {
        this.key = key;
        this.value = value;
        this.nullable = nullable;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    public boolean isNullable() {
        return nullable;
    }

    public static Object getOrDefault(Map config, RedisEnumConfig defaultConfig) {
        if(BaseUtil.isEmpty(config)) {
            throw new LockConfigException("redisConfig is invalid, please make sure that config [redis] exist below root config [lock]");
        }

        if(BaseUtil.isEmpty(config.get(defaultConfig.getKey()))) {
            if(!defaultConfig.isNullable()) {
                throw new LockConfigException("redisConfig is invalid, please make sure that config [redis."+defaultConfig.getKey()+"] exist below config [lock.redis]");
            }
            return defaultConfig.getValue();
        }

        return config.get(defaultConfig.getKey());
    }

    public enum RedisSingleEnumConfig {
        /**
         * redis的连接地址
         */
        CONNECTION_HOST ("host", "", false),

        /**
         * redis的连接端口
         */
        CONNECTION_PORT ("port", "", false)
        ;

        private String key; // 配置的key
        private Object value; // 配置的value
        private boolean nullable; // 是否可以为空

        RedisSingleEnumConfig(String key, Object value, boolean nullable) {
            this.key = key;
            this.value = value;
            this.nullable = nullable;
        }

        public String getKey() {
            return key;
        }

        public Object getValue() {
            return value;
        }

        public boolean isNullable() {
            return nullable;
        }

        public static Object getOrDefault(Map config, RedisSingleEnumConfig defaultConfig) {
            if(BaseUtil.isEmpty(config)) {
                throw new LockConfigException("redisSingleConfig is invalid, please make sure that config [redis.single] exist below root config [lock]");
            }

            if(BaseUtil.isEmpty(config.get(defaultConfig.getKey()))) {
                if(!defaultConfig.isNullable()) {
                    throw new LockConfigException("redisSingleConfig is invalid, please make sure that config [single."+defaultConfig.getKey()+"] exist below config [lock.redis]");
                }
                return defaultConfig.getValue();
            }

            return config.get(defaultConfig.getKey());
        }
    }

    public enum RedisSentinelEnumConfig {
        /**
         * redis的连接地址
         */
        MASTER_NAME ("masterName", "redisMasterNode", true),

        /**
         * redis的连接端口
         */
        ADDRESSES ("addresses", null, false)
        ;

        private String key; // 配置的key
        private Object value; // 配置的value
        private boolean nullable; // 是否可以为空

        RedisSentinelEnumConfig(String key, Object value, boolean nullable) {
            this.key = key;
            this.value = value;
            this.nullable = nullable;
        }

        public String getKey() {
            return key;
        }

        public Object getValue() {
            return value;
        }

        public boolean isNullable() {
            return nullable;
        }

        public static Object getOrDefault(Map config, RedisSentinelEnumConfig defaultConfig) {
            if(BaseUtil.isEmpty(config)) {
                throw new LockConfigException("redisSentinelConfig is invalid, please make sure that config [redis.sentinel] exist below root config [lock]");
            }

            if(BaseUtil.isEmpty(config.get(defaultConfig.getKey()))) {
                if(!defaultConfig.isNullable()) {
                    throw new LockConfigException("redisSentinelConfig is invalid, please make sure that config [sentinel."+defaultConfig.getKey()+"] exist below config [lock.redis]");
                }
                return defaultConfig.getValue();
            }

            return config.get(defaultConfig.getKey());
        }
    }

    public enum RedisClusterEnumConfig {
        /**
         * redis的连接地址
         */
        SCAN_INTERVAL ("scanInterval", 2000, true),

        /**
         * redis的连接端口
         */
        ADDRESSES ("addresses", null, false)
        ;

        private String key; // 配置的key
        private Object value; // 配置的value
        private boolean nullable; // 是否可以为空

        RedisClusterEnumConfig(String key, Object value, boolean nullable) {
            this.key = key;
            this.value = value;
            this.nullable = nullable;
        }

        public String getKey() {
            return key;
        }

        public Object getValue() {
            return value;
        }

        public boolean isNullable() {
            return nullable;
        }

        public static Object getOrDefault(Map config, RedisClusterEnumConfig defaultConfig) {
            if(BaseUtil.isEmpty(config)) {
                throw new LockConfigException("redisClusterConfig is invalid, please make sure that config [redis.cluster] exist below root config [lock]");
            }

            if(BaseUtil.isEmpty(config.get(defaultConfig.getKey()))) {
                if(!defaultConfig.isNullable()) {
                    throw new LockConfigException("redisClusterConfig is invalid, please make sure that config [cluster."+defaultConfig.getKey()+"] exist below config [lock.redis]");
                }
                return defaultConfig.getValue();
            }

            return config.get(defaultConfig.getKey());
        }
    }

}
