package com.qingzhu.component.lock.common.constant.cache.strategy;

/**
 * 当指定业务执行过期时间，业务仍然没有执行完毕的执行策略
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/22 10:13
 */
public class LockExpireStrategyTypeConstant {

    /**
     * 直接抛异常
     */
    public final static int LOCK_EXPIRE_STRATEGY_EXCEPTION = 0;

    /**
     * 直接忽略，啥都不干，业务执行一半就一半
     */
    public final static int LOCK_EXPIRE_STRATEGY_IGNORE = 1;

}
