package com.qingzhu.component.lock.starter.scanner;

import com.qingzhu.component.lock.common.exception.LockConfigException;
import com.qingzhu.component.lock.common.util.BaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.springframework.core.annotation.AnnotatedElementUtils.findMergedAnnotation;
import static org.springframework.core.annotation.AnnotationUtils.getAnnotationAttributes;
import static org.springframework.util.ClassUtils.resolveClassName;

/**
 * 自定义容器的bd扫描器
 * @author xiangjz
 * @version 1.0
 * @date 2020/9/4 9:42
 */
@Slf4j
public class EmptyLockComponentBeanDefinitionScanner extends ClassPathBeanDefinitionScanner {

    private Environment environment;

    private ClassLoader classLoader;

    private List<Class<? extends Annotation>> serviceAnnotationTypes;

    public EmptyLockComponentBeanDefinitionScanner(BeanDefinitionRegistry registry,
                                                   Environment environment, ClassLoader classLoader,
                                                   List<Class<? extends Annotation>> serviceAnnotationTypes) {
        super(registry);
        this.environment = environment;
        this.classLoader = classLoader;
        this.serviceAnnotationTypes = serviceAnnotationTypes;
    }

    @Override
    public Set<BeanDefinitionHolder> doScan(String... basePackages) {
        return super.doScan(basePackages);
    }

    @Override
    public boolean checkCandidate(String beanName, BeanDefinition beanDefinition) throws IllegalStateException {

        if(beanDefinition instanceof AnnotatedBeanDefinition) {

            Annotation annotation = serviceAnnotationTypes
                    .stream()
                    .map(annotationType -> findMergedAnnotation(resolveClassName(beanDefinition.getBeanClassName(), classLoader), annotationType))
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(null);

            if(BaseUtil.isEmpty(annotation)) {
                // 该bean没有被@EmptyCommonBeanAnnotation
                return false;
            }
        }

        return super.checkCandidate(beanName, beanDefinition);
    }

    @Override
    public void setBeanNameGenerator(BeanNameGenerator beanNameGenerator) {
        super.setBeanNameGenerator(beanNameGenerator);
    }

    @Override
    public void registerBeanDefinition(BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry) {
        super.registerBeanDefinition(definitionHolder, registry);
    }

}
