package com.qingzhu.component.lock.starter.scanner;

import com.qingzhu.component.lock.common.constant.cache.strategy.LockCacheOutStrategyTypeConstant;
import com.qingzhu.component.lock.common.util.BaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.springframework.core.annotation.AnnotatedElementUtils.findMergedAnnotation;
import static org.springframework.core.annotation.AnnotationUtils.getAnnotationAttributes;
import static org.springframework.util.ClassUtils.resolveClassName;

/**
 * 锁对象淘汰的bd扫描器
 * @author xiangjz
 * @version 1.0
 * @date 2020/9/4 9:42
 */
@Slf4j
public class LockCacheOutStrategyBeanDefinitionScanner extends ClassPathBeanDefinitionScanner {

    private Environment environment;

    private ClassLoader classLoader;

    private List<Class<? extends Annotation>> serviceAnnotationTypes;

    public LockCacheOutStrategyBeanDefinitionScanner(BeanDefinitionRegistry registry,
                                                     Environment environment, ClassLoader classLoader,
                                                     List<Class<? extends Annotation>> serviceAnnotationTypes) {
        super(registry);
        this.environment = environment;
        this.classLoader = classLoader;
        this.serviceAnnotationTypes = serviceAnnotationTypes;
    }

    @Override
    public Set<BeanDefinitionHolder> doScan(String... basePackages) {
        return super.doScan(basePackages);
    }

    @Override
    public boolean checkCandidate(String beanName, BeanDefinition beanDefinition) throws IllegalStateException {
        String strategyForCapacity = environment.getProperty("lock.strategyForCapacity");
        if(BaseUtil.isEmpty(strategyForCapacity)) {
            strategyForCapacity = LockCacheOutStrategyTypeConstant.LOCK_CACHE_OUT_STRATEGY_LFU;
        }

        if(beanDefinition instanceof AnnotatedBeanDefinition) {

            Annotation annotation = serviceAnnotationTypes
                    .stream()
                    .map(annotationType -> findMergedAnnotation(resolveClassName(beanDefinition.getBeanClassName(), classLoader), annotationType))
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(null);

            if(BaseUtil.isEmpty(annotation)) {
                // 该bean没有被@LockTypeBeanAnnotation
                return super.checkCandidate(beanName, beanDefinition);
            }

            AnnotationAttributes serviceAnnotationAttributes = getAnnotationAttributes(annotation, false, false);

            Object value = BaseUtil.isEmpty(serviceAnnotationAttributes) ? "" : serviceAnnotationAttributes.get("type");
            return !BaseUtil.isEmpty(value) && value.equals(strategyForCapacity);
        }

        return super.checkCandidate(beanName, beanDefinition);
    }

    @Override
    public void setBeanNameGenerator(BeanNameGenerator beanNameGenerator) {
        super.setBeanNameGenerator(beanNameGenerator);
    }

    @Override
    public void registerBeanDefinition(BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry) {
        super.registerBeanDefinition(definitionHolder, registry);
    }

}
