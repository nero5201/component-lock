package com.qingzhu.component.lock.starter.scanner;

import com.qingzhu.component.lock.common.exception.LockConfigException;
import com.qingzhu.component.lock.common.util.BaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.springframework.core.annotation.AnnotatedElementUtils.findMergedAnnotation;
import static org.springframework.core.annotation.AnnotationUtils.getAnnotationAttributes;
import static org.springframework.util.ClassUtils.resolveClassName;

/**
 * 自定义容器的bd扫描器
 * @author xiangjz
 * @version 1.0
 * @date 2020/9/4 9:42
 */
@Slf4j
public class LockComponentBeanDefinitionScanner extends ClassPathBeanDefinitionScanner {

    private Environment environment;

    private ClassLoader classLoader;

    private List<Class<? extends Annotation>> serviceAnnotationTypes;

    public LockComponentBeanDefinitionScanner(BeanDefinitionRegistry registry,
                                              Environment environment, ClassLoader classLoader,
                                              List<Class<? extends Annotation>> serviceAnnotationTypes) {
        super(registry);
        this.environment = environment;
        this.classLoader = classLoader;
        this.serviceAnnotationTypes = serviceAnnotationTypes;
    }

    @Override
    public Set<BeanDefinitionHolder> doScan(String... basePackages) {
        return super.doScan(basePackages);
    }

    @Override
    public boolean checkCandidate(String beanName, BeanDefinition beanDefinition) throws IllegalStateException {
        /*
         * 这里主要是判断，防止加载不需要的lockBean，防止不需要的第三方中间件的依赖
         * 通过@LockTypeBeanAnnotation注解，以及其type字段是否与配置文件中的lock.type字段匹配
         */
        String type = environment.getProperty("lock.type");
        if(BaseUtil.isEmpty(type)) {
            throw new LockConfigException("lock.type could not be empty");
        }

        if(beanDefinition instanceof AnnotatedBeanDefinition) {

            Annotation annotation = serviceAnnotationTypes
                    .stream()
                    .map(annotationType -> findMergedAnnotation(resolveClassName(beanDefinition.getBeanClassName(), classLoader), annotationType))
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(null);

            if(BaseUtil.isEmpty(annotation)) {
                // 该bean没有被@LockTypeBeanAnnotation
                return super.checkCandidate(beanName, beanDefinition);
            }

            AnnotationAttributes serviceAnnotationAttributes = getAnnotationAttributes(annotation, false, false);

            // 这里检查bean的注解，是否有被@LockTypeBeanAnnotation及其关联注解annotate
            // 如果有的话，再检查其value是否与配置的type一致，不一致的话不允许添加到bd，防止加载多余的bean
            Object value = BaseUtil.isEmpty(serviceAnnotationAttributes) ? "" : serviceAnnotationAttributes.get("type");
            return !BaseUtil.isEmpty(value) && value.equals(type);
        }

        return super.checkCandidate(beanName, beanDefinition);
    }

    @Override
    public void setBeanNameGenerator(BeanNameGenerator beanNameGenerator) {
        super.setBeanNameGenerator(beanNameGenerator);
    }

    @Override
    public void registerBeanDefinition(BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry) {
        super.registerBeanDefinition(definitionHolder, registry);
    }

}
