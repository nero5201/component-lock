package com.qingzhu.component.lock.way.juc.generator;

import com.qingzhu.component.lock.annotation.generator.LockGeneratorBean;
import com.qingzhu.component.lock.common.cache.LockCache;
import com.qingzhu.component.lock.common.cache.expire.strategy.CacheOutStrategy;
import com.qingzhu.component.lock.common.constant.locktype.LockTypeConstant;
import com.qingzhu.component.lock.common.entity.LockEntity;
import com.qingzhu.component.lock.common.generator.LockGenerator;
import com.qingzhu.component.lock.common.pool.ForkLockExecutor;
import com.qingzhu.component.lock.common.util.BaseUtil;
import com.qingzhu.component.lock.way.juc.entity.JucLockEntity;

/**
 * juc的锁生成器
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/14 17:09
 */
@LockGeneratorBean(value = "jucLockGenerator", type = LockTypeConstant.LOCK_TYPE_JUC)
public class JucLockGenerator extends LockGenerator<String> {

    public JucLockGenerator(CacheOutStrategy cacheOutStrategy,
                            ForkLockExecutor forkLockExecutor) {
        super(cacheOutStrategy, forkLockExecutor);
    }

    @Override
    public LockEntity doGetLock(String key, long expireTime) {
        return new JucLockEntity(key, expireTime);
    }
}
