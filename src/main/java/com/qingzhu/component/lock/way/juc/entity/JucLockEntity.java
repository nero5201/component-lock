package com.qingzhu.component.lock.way.juc.entity;

import com.qingzhu.component.lock.common.entity.LockEntity;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * juc 的锁实例封装
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/14 17:07
 */
@Slf4j
public class JucLockEntity extends LockEntity<String> {

    private Lock lock;

    public JucLockEntity(String lockKey, long expireTime) {
        super(lockKey, expireTime);
        this.lock = new ReentrantLock();
    }

    @Override
    public boolean doLock() {
        this.lock.lock();
        return true;
    }

    @Override
    public void doUnlock() {
        this.lock.unlock();
    }

}
