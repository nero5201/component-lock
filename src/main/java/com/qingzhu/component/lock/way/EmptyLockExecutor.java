package com.qingzhu.component.lock.way;

import com.qingzhu.component.lock.annotation.LockCommonBeanAnnotation;
import com.qingzhu.component.lock.annotation.starter.EmptyCommonBeanAnnotation;
import com.qingzhu.component.lock.common.constant.AutoConfig;
import com.qingzhu.component.lock.common.entity.LockEntity;
import com.qingzhu.component.lock.common.generator.LockGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

/**
 * 锁的执行器，当LockExecutor没有被注入的时候(说明用户取消了使用该插件)，则代替LockExecutor进行注入，防止运行时报错
 * @author xiangjz
 * @version 1.0
 * @date 2021/3/18 17:37
 */
@ConditionalOnProperty(prefix = AutoConfig.CONFIG_PREFIX, name = "enable", havingValue = "true", matchIfMissing = true)
@EmptyCommonBeanAnnotation("lockExecutor")
@Slf4j
public class EmptyLockExecutor extends LockExecutor {

    @Autowired
    public EmptyLockExecutor() {
        super(null);
    }

    public EmptyLockExecutor(LockGenerator lockGenerator) {
        super(lockGenerator);
    }

    @Override
    public LockEntity lock(String key) {
        return new LockEntity(key, -1) {
            @Override
            public boolean doLock() {
                return false;
            }

            @Override
            public void doUnlock() {

            }
        };
    }

    @Override
    public void unlock(String key) {

    }
}
