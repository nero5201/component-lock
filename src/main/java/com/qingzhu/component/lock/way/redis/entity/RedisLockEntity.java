package com.qingzhu.component.lock.way.redis.entity;

import com.qingzhu.component.lock.common.entity.LockEntity;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

/**
 * redis 的锁实例封装
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/14 17:07
 */
@Slf4j
public class RedisLockEntity extends LockEntity<String> {

    private RLock rLock;

    public RedisLockEntity(String lockKey, long expireTime) {
        this(lockKey, expireTime, null, null);
    }

    public RedisLockEntity(String lockKey, long expireTime, RedissonClient redissonClient, String lockName) {
        super(lockKey, expireTime);
        rLock = redissonClient.getLock(lockName);
    }

    @Override
    public boolean doLock() {
        this.rLock.lock();
        return true;
    }

    @Override
    public void doUnlock() {
        this.rLock.unlock();
    }

}
