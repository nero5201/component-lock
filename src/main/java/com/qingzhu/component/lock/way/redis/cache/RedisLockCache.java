package com.qingzhu.component.lock.way.redis.cache;

import com.qingzhu.component.lock.annotation.cache.LockCacheBean;
import com.qingzhu.component.lock.common.cache.LockCache;
import com.qingzhu.component.lock.common.constant.locktype.LockTypeConstant;
import com.qingzhu.component.lock.common.entity.LockEntity;
import com.qingzhu.component.lock.way.redis.generator.RedisLockGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * redis 的锁缓存机制
 * 利用CHM
 * @author xiangjz
 * @version 1.0
 * @date 2021/1/14 17:19
 */
@ConditionalOnClass(RedisLockGenerator.class)
@LockCacheBean(value = "redisLockCache", type = LockTypeConstant.LOCK_TYPE_REDIS)
public class RedisLockCache implements LockCache<String> {

    private final int cacheCapacity;

    /**
     * 业务 <-> 锁的映射关系缓存
     * 每个不同锁机制的缓存由它们自己维护，不需要将lockCache提升到lockGenerator层，尽管它们的逻辑可能一样
     */
    private Map<String, LockEntity> lockCache;

    public RedisLockCache(@Value("${lock.lockCacheCapacity}") int cacheCapacity) {
        this.lockCache = new ConcurrentHashMap<>(cacheCapacity);
        this.cacheCapacity = cacheCapacity == 0 ? 128 : cacheCapacity;
    }

    @Override
    public LockEntity cache(String key, LockEntity lock) {
        lockCache.put(key, lock);
        return lock;
    }

    @Override
    public LockEntity cacheIfAbsent(String key, LockEntity lock) {
        lockCache.putIfAbsent(key, lock);
        return lockCache.get(key);
    }

    @Override
    public void removeCache(String key) {
        lockCache.remove(key);
    }

    @Override
    public LockEntity get(String key) {
        return this.lockCache.get(key);
    }

    @Override
    public int getCacheCapacity() {
        return cacheCapacity;
    }

    @Override
    public int getCacheSize() {
        return this.lockCache.size();
    }

}
