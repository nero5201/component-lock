package com.qingzhu.component.lock.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 锁的类型配置注解
 * @author xiangjz
 * @version 1.0
 * @date 2020/9/2 14:22
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@LockCommonBeanAnnotation
public @interface LockTypeBeanAnnotation {

    /**
     * 根据锁的类型，自定义的beanName
     * 需要注意的是，这个beanName必须是type + 其父类或者接口的simpleClassName
     * 如：LockCache的实现jucLockCache
     * @return
     */
    @AliasFor(annotation = LockCommonBeanAnnotation.class)
    String value() default "";

    /**
     * 这是指定的锁的类型，lock.type
     * @return
     */
    String type() default "";
}
