package com.qingzhu.component.lock.annotation.starter;

import java.lang.annotation.*;

/**
 * lock组件自定义类的bean扫描器
 * @author xiangjz
 * @version 1.0
 * @date 2020/9/4 9:30
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
//@Import({LockConponentAutoConfiguration.class})
public @interface LockComponentScan {

    String[] value() default {};

    String[] basePackages() default {};

    Class<?>[] basePackageClasses() default {};
}
