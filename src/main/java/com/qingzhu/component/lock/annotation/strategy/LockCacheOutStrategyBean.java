package com.qingzhu.component.lock.annotation.strategy;

import com.qingzhu.component.lock.annotation.LockCommonBeanAnnotation;
import com.qingzhu.component.lock.annotation.LockTypeBeanAnnotation;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 标记自定义的LockCacheOutStrategy，并能够制定注入到spring容器中的beanName
 * @author xiangjz
 * @version 1.0
 * @date 2020/8/28 15:55
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface LockCacheOutStrategyBean {

    String value() default "";

    String type() default "";
}
