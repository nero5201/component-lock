package com.qingzhu.component.lock.annotation.business;

import com.qingzhu.component.lock.common.constant.cache.strategy.LockExpireStrategyTypeConstant;

import java.lang.annotation.*;

/**
 * 申请加锁的注解，注解于方法上，则整个方法会根据配置锁的类型进行加锁
 * @author xiangjz
 * @version 1.0
 * @date 2020/9/3 11:03
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface LockAcquire {

    /**
     * 自定义的业务lock.key
     * @return
     */
    String key() default "";

    /**
     * 锁的过期的时间
     * 默认永不过期
     * 单位 ms
     * @return
     */
    long expire() default -1L;

    /**
     * 当锁超时仍没有被释放释放时的回调类型
     * @return
     */
    int strategyOfExpired() default LockExpireStrategyTypeConstant.LOCK_EXPIRE_STRATEGY_IGNORE;

    /**
     * 当expire > 0，当超时时，是否立即停止尚未完成的业务逻辑
     * @return
     */
    boolean stopOnExpired() default false;

    /**
     * 当前进程中，fork的子线程之前尝试申请资源的耗时(ms)
     * 若<0，则视为永久等待可用资源
     * @return
     */
    long expireOnForkingFull() default -1L;

}
