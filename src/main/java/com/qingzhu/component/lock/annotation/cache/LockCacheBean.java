package com.qingzhu.component.lock.annotation.cache;

import com.qingzhu.component.lock.annotation.LockTypeBeanAnnotation;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 标记自定义的LockCache，并能够制定注入到spring容器中的beanName
 * @author xiangjz
 * @version 1.0
 * @date 2020/8/28 15:55
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@LockTypeBeanAnnotation
public @interface LockCacheBean {

    @AliasFor(annotation = LockTypeBeanAnnotation.class)
    String value() default "";

    @AliasFor(annotation = LockTypeBeanAnnotation.class)
    String type() default "";
}
