package com.qingzhu.component.lock.annotation.starter;

import java.lang.annotation.*;

/**
 * @author xiangjz
 * @version 1.0
 * @date 2020/9/2 14:22
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface EmptyCommonBeanAnnotation {

    String value() default "";
}
