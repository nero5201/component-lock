package com.qingzhu.component.lock.annotation.business;

import java.lang.annotation.*;

/**
 * 申请加锁的注解，注解于方法参数上，需要在该方法上有 LockAcquire 注解
 * @author xiangjz
 * @version 1.0
 * @date 2020/9/3 11:03
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
@Inherited
public @interface LockCustomParam {

    /**
     * 根据参数字段值而动态拼接的key
     * 同一个注解中，优先级高于keys
     * @return
     */
    String key() default "";

    /**
     * key() 拼接成lock.key的顺序
     * 当有多个LockCustomParam注解时，order升序拼接
     * @return
     */
    int order() default 0;

    /**
     * 注解于一个参数，但需要指定参数中多个字段时
     * 按照数组顺序，进行lock.key的拼接
     * 同一个注解中，优先级低于key
     * @return
     */
    String[] keys() default "";
}
